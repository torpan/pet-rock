import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;


public class PetRockTest
{
    private PetRock rocky;

    @BeforeEach
    public void myTestSetUp() throws Exception
    {
        rocky = new PetRock("Rocky");
    }

    @Test
    public void getName()
    {
        assertEquals("Rocky", rocky.getName());
    }
    @Test
    public void testUnhappyToStart()
    {
        assertFalse(rocky.isHappy());
    }
    @Test
    public void testHappyAfterPlay()
    {
        rocky.playWithRock();
        assertTrue(rocky.isHappy());
    }

    @Disabled ("Exception throwing not yet defined.")
    @Test
    public void nameFail()
    {
        Assertions.assertThrows(IllegalStateException.class, () -> {
            rocky.getHappyMessage();
        });
    }
    @Test
    public void name()
    {
        rocky.playWithRock();
        String msg = rocky.getHappyMessage();
        assertEquals("I'm happy!", msg);
    }

    @Test
    public void testFavNum()
    {
        assertEquals(42, rocky.getFavNumber());
    }

    @Test
    public void emptyNameFail()
    {
        Assertions.assertThrows(IllegalStateException.class, () -> {
            new PetRock(" ");
        });
    }

}